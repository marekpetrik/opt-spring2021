# MATH/CS 757/857: Mathematical Optimization for Applications #

### When and Where

Lectures: MW 9:40am - 11am

Recitations: F 9:40am - 11am, online only, led by Soheil

Where: Kingsbury N334 and Online (zoom via mycourses)

**Class recordings:** At least initially, I will share class recordings only with students who have pre-arranged their absence with me and have a valid reason for not attending at the time of the lecture. However, neither the lectures nor the recitations are mandatory.

See [class overview](overview/overview.pdf) for the information on grading, rules, and office hours.

## Syllabus: Lectures

The slides will be uploaded after each lecture, including the class notes

Regarding *reading materials* see the section on textbooks below. *NP* is the main textbook.

| Date   | Day | Slides                                                                | Reading |
| ------ | --- | --------------------------------------------------------------------- | ------- |
| Feb 01 | Mon | [Introduction](slides/class1/class1_ann.pdf)                          | NP 1.1  |
| Feb 03 | Wed | [Optimality conditions](slides/class2/class2_ann.pdf)                 | NP 1.1  |
| Feb 08 | Mon | [Gradient methods: convergence](slides/class3/class3_ann.pdf)         | NP 1.2  |
| Feb 10 | Wed | [Gradient methods: convergence rate](slides/class4/class4_ann.pdf)    | NP 1.3  |
| Feb 15 | Mon | [Newton's method](slides/class5/class5_ann.pdf)                       | NP 1.4  |
| Feb 17 | Wed | [Newton's method](slides/class6/class6_ann.pdf)                       | NP 1.4  |
| Feb 22 | Mon | [Conjugate gradient](slides/class7/class7_ann.pdf)                    | NP 2.1  |
| Feb 24 | Wed | [Conjugate gradient](slides/class7/class7_ann.pdf)                    | NP 2.1,2|
| Mar 01 | Mon | [Quasi-Newton methods](slides/class8/class8_ann.pdf)                  | NP 2.2  |
| Mar 03 | Wed | [Review](slides/class9/class9_ann.pdf)                                |         |
| Mar 08 | Mon | No lecture; a virtual Friday                                          |         |
| Mar 10 | Wed | **Midterm** (early)                                                   |         |
| Mar 15 | Mon | [Convex constrained optimization](slides/class10/class10_ann.pdf)     | NP 3.1  |
| Mar 17 | Wed | [Convex constrained optimization](slides/class11/class11_ann.pdf)     | NP 3.1  |
| Mar 22 | Mon | [Projected gradient method](slides/class12/class12_ann.pdf)           | NP 3.2,3|
| Mar 24 | Wed | [Proximal algorithms](slides/class13/class13_ann.pdf)                 | NP 3.6  |
| Mar 29 | Mon | [Lagrange multipliers](slides/class14/class14_ann.pdf)                | NP 4.1  |
| Mar 31 | Wed | [Sufficiency and sensitivity](slides/class15/class15_ann.pdf)         | NP 4.2  |
| Apr 05 | Mon | [KKT conditions](slides/class16/class16_ann.pdf)                      | NP 4.3  |
| Apr 07 | Wed | [KKT conditions](slides/class17/class17_ann.pdf)                      | NP 4.3  |
| Apr 12 | Mon | [Linear constraints and duality](slides/class18/class18_ann.pdf)      | NP 4.4  |
| Apr 14 | Wed | [Duality, linear](slides/class19/class19_ann.pdf)                     | NP 4.4  |
| Apr 19 | Mon | [Duality and dual problems](slides/class20/class20_ann.pdf)           | NP 6.1  |
| Apr 21 | Wed | [Weak duality, LP](slides/class21/class21_ann.pdf)                    | NP 6.1  |
| Apr 26 | Mon | [Strong duality, linear programming](slides/class22/class22_ann.pdf)  | NP 6.2,3|
| Apr 28 | Wed | [Linear programming, barrier/penalty](slides/class23/class23_ann.pdf) | NP 6.4  |
| May 03 | Mon | Project presentations                                                 |         |
| May 05 | Wed | Project presentations and discussion                                  |         |
| May 10 | Mon | [Final exam review](slides/class24/class24.pdf)                       |         |

## Office Hours

*Piazza*: [piazza.com/unh/spring2021/csmath757857](piazza.com/unh/spring2021/csmath757857)

  - *Marek*: Tue 8:30-9:30am at [https://unh.zoom.us/j/5833938643](https://unh.zoom.us/j/5833938643)
  - *Soheil (TA)*: Fri 2-3pm at [https://unh.zoom.us/j/92604812439 ](https://unh.zoom.us/j/92604812439)

## Assignments

| Assignment                        | Source                            |  Due Date           |
|---------------------------------- | --------------------------------- | --------------------|
| [1](assignments/assignment1.pdf)  | [1](assignments/assignment1.tex)  | Mon 2/08 at 11:59PM |
| [2](assignments/assignment2.pdf)  | [2](assignments/assignment2.tex)  | Mon 2/15 at 11:59PM |
| [3](assignments/assignment3.pdf)  | [3](assignments/assignment3.tex)  | Tue 2/23 at 11:59PM |
| [4](assignments/assignment4.pdf)  | [4](assignments/assignment4.tex)  | Mon 3/08 at 11:59PM |
| [5](assignments/assignment5.pdf)  | [5](assignments/assignment5.tex)  | Thu 3/25 at 11:59PM |
| [6](assignments/assignment6.pdf)  | [6](assignments/assignment6.tex)  | Wed 3/31 at 11:59PM |
| [7](assignments/assignment7.pdf)  | [7](assignments/assignment7.tex)  | Tue 4/06 at 11:59PM |
| [8](assignments/assignment8.pdf)  | [8](assignments/assignment8.tex)  | Tue 4/13 at 11:59PM |
| [9](assignments/assignment9.pdf)  | [9](assignments/assignment9.tex)  | Tue 4/20 at 11:59PM |
| [10](assignments/assignment10.pdf)| [10](assignments/assignment10.tex)| Tue 4/27 at 11:59PM |
| [11](assignments/assignment11.pdf)| [11](assignments/assignment11.tex)| Tue 5/04 at 11:59PM |
| [12](assignments/assignment12.pdf)| [12](assignments/assignment12.tex)| Tue 5/10 at  8:00AM |

## Project

The project will be individual and will require about the same amount of effort as a single assignment.

## Exams

Both midterm and final exams will be a take-home (final during the finals week). The exams will be based on conceptual/theoretical questions similar to homework assignments.

## Textbooks ##

### Main Reference:
- **NP**: Bertsekas, D. (2016) Nonlinear programming, 3rd edition.


### Linear Algebra:
- **LA**: Strang, G. [Introduction to Linear Algebra](http://math.mit.edu/~gs/linearalgebra/). (2016) *Also see:* [online lectures](https://ocw.mit.edu/courses/mathematics/18-06-linear-algebra-spring-2010/video-lectures/)
- **LAO**: Hefferon, J. [Linear Algebra](http://joshua.smcvt.edu/linearalgebra/#current_version) (2017)


### Mathematical Optimization:
- **CO** Boyd, S., & Vandenberghe, L. (2004). [Convex Optimization](http://web.stanford.edu/~boyd/cvxbook/)
- **NO** Nocedal, J., Wright, S. (2006). [Numerical Optimization](https://www.springer.com/us/book/9780387303031)

## Programming Language

We will use Python or Julia in the class end recitations. You can use also MATLAB or R or check with the instructor (piazza) about other options.

## Pre-requisites ##

At least some linear algebra and calculus.
