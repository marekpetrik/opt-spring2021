\documentclass[9pt]{beamer}

\usepackage{mathtools}
\usepackage{amsfonts}
%\usepackage{eucal}
\usepackage{calrsfs}

\usepackage{array}
%\usepackage{enumitem}


\title{Mathematical Optimization: Gradient Descent Convergence}
\subtitle{CS/MATH 757/857}
\author{Marek Petrik}
\date{February 10}

\newcommand{\opt}{^\star}
\newcommand{\tr}{^\intercal}
\newcommand{\st}{\quad\operatorname{s.t.}\quad}
\newcommand{\eye}{I}
\newenvironment{mprog}{\begin{array}{>{\displaystyle}l>{\displaystyle}l>{\displaystyle}l}}{\end{array}}
\newcommand{\subjectto}{\operatorname{s.\,t.} &}
\newcommand{\minimize}[1]{\min_{#1} &}
\newcommand{\maximize}[1]{\max_{#1} &}
\newcommand{\cs}{\\[1ex] & }
\newcommand{\stc}{\\[1ex] \subjectto}
\newcommand{\real}{\mathbb{R}}


\setbeamertemplate{navigation symbols}{}
\usefonttheme{professionalfonts}
\setbeamertemplate{footline}[frame number]

\begin{document}

\begin{frame}
	\maketitle
\end{frame}

\begin{frame}{Linear Algebra: Appendix A}
    Vector norm
    \[ \| x \| = \hspace{5cm} \]
    \vfill
    Induced matrix norm
    \[ \| A \| = \hspace{5cm} \]
    \vfill
    Eigenvalues and eigenvectors
    \[  A e = \lambda \cdot e \]
    \vfill
    \textbf{Eigendecomposition of a symmetric matrix}
    \[ A = \hspace{5cm} \]
    \vfill
\end{frame}

\begin{frame}{Symmetric Matrices}
    If $A$ is symmetric with eigenvalues $\lambda_1 \le \lambda_2 \le \ldots \le \lambda_n$ then:\\
    \vfill
    \begin{enumerate}
        \item $\|A \| = \max\{ |\lambda_1| , |\lambda_n| \}$
        \vfill
        \item Quadratic form satisfies for each $y$:
        \[ \lambda_1 \le \frac{y\tr A y}{\|y\|^2} \le \lambda_n \]
    \end{enumerate}


\end{frame}

\begin{frame}{Last Time: Gradient Descent}
    \[ x^{k+1} = x^k + \alpha^k d^k \]
    \begin{enumerate}
        \item Step direction
        \vfill
        \item Step size
    \end{enumerate}
\end{frame}

\begin{frame}{Today: Gradient descent}
    \Large
    \begin{itemize}
        \item \textbf{Termination}
        \vfill
        \item Convergence
        \vfill
        \item Convergence rate
    \end{itemize}
\end{frame}


\begin{frame}{Termination}
    \[ \| \nabla f(x^k) \| \le \epsilon  \]
    \vspace{4cm}
    \[  \frac{\| \nabla f(x^k) \|}{\| \nabla f(x^0) \|} \le \epsilon \]
\end{frame}

\begin{frame}{How meaningful is termination condition?}
    \[ f(x) = \frac{1}{2} \cdot \epsilon \cdot x \]
    \vspace{3cm}
    \[ \frac{z\tr \nabla^2 f(x) z}{\|z\|^2} \ge m \]
    Then
    \[ \| x - x\opt \| \le \frac{\epsilon}{m}, \qquad f(x) - f(x\opt) \le \frac{\epsilon^2}{m} \]
\end{frame}

\begin{frame}{Today: Gradient descent}
    \Large
    \begin{itemize}
        \item Termination
        \vfill
        \item \textbf{Convergence}
        \vfill
        \item Convergence rate
    \end{itemize}
\end{frame}

\begin{frame}{Today: Gradient Descent}
    \[ x^{k+1} \;=\; x^k - \alpha^k D^k \nabla f(x^k)\]
    \begin{enumerate}
        \item Direction
        \vfill
        \item Step size
        \vfill
        \item \textbf{Convergence}
    \end{enumerate}
\end{frame}

\begin{frame}{Line search}
    If $\nabla f(x^k) d^k < 0$ for nonstationary points \textbf{and} $\alpha^k$ is chosen by (limited) minimization rule or Armijo rule, then (if it exists):
    \[ \lim_{k\to\infty} \nabla f(x^k) = 0 ~.\]
    \vspace{5cm}
\end{frame}

\begin{frame}{Constant stepsize}
    We get (if it exists)
    \[ \lim_{k\to\infty} \nabla f(x^k) = 0 \]
    when
    \begin{enumerate}
        \item Lipschitz condition:
        \[ \nabla f(x) - \nabla f(y) \le L \| x - y \| \]
        \item $\epsilon \le \alpha^k \le (2-\epsilon) \bar{\alpha}^k$ and
        \[ \bar{\alpha}^k = \frac{|\nabla f(x^k)\tr d^k|}{L \cdot d^k} \]
    \end{enumerate}
\end{frame}


\begin{frame}{Diminishing stepsize}
    We get (if it exists)
    \[ \lim_{k\to\infty} \nabla f(x^k) = 0 \]
    when
    \begin{enumerate}
        \item Lipschitz condition (continuity):
        \[ \nabla f(x) - \nabla f(y) \le L \| x - y \| \]
        \item Exist $c_1 > 0$ and $c_2 > 0$ such that
        \[ c_1 \| \nabla f(x^k) \|^2 \le -\nabla f(x^k)\tr d^k, \qquad \|d^k\|^2 \le c_2 \| f(x^k) \|^2\]
        \item Stepsize satisfies:
        \[ \alpha^k \to 0, \qquad \sum_{k=0}^{\infty} \alpha^k = \infty \]
    \end{enumerate}
\end{frame}

\begin{frame}{Capture theorem}
    If $f(x^{k+1}) \le f(x^k)$ and $x^{k+1} = x^k + \alpha^k d^k$ and $x^k \to $ stationary point and
    \begin{enumerate}
        \item Exist $s >0 $ and $c > 0$ such that $\forall k$
        \[ \alpha^k \le s, \qquad \| d^k \| \le \| \nabla f(x^k) \| \]
        \item $x\opt$ is a stationary point in some open $\mathcal{S}$
        \item $x^{\bar{k}} \in \mathcal{S}$ for some $\bar{k}$
    \end{enumerate}
    Then:
    \[
    x^k \to x\opt~.
    \]
\end{frame}

\begin{frame}{Today: Gradient descent}
    \Large
    \begin{itemize}
        \item Termination
        \vfill
        \item Convergence
        \vfill
        \item \textbf{Convergence rate}
    \end{itemize}
\end{frame}

\begin{frame}{Complexity analysis}
    \begin{enumerate}
        \item Computational complexity
        \vfill
        \item Informational complexity
        \vfill
        \item Local complexity
    \end{enumerate}
\end{frame}


\begin{frame}{Convergence rates}
    Sequence ${x^0, x^1, x^2, \ldots} \to x\opt$\\
    \vfill
    Error $e(x)$ \\
    \vfill
    \begin{enumerate}
        \item Linear
        \vfill
        \item Quadratic
        \vfill
        \item Superlinear
        \vfill
    \end{enumerate}

\end{frame}

\begin{frame}{Local convergence analysis}
    \[ f(x) = f(x\opt) + (x - x\opt)\tr \nabla f(x\opt) + \frac{1}{2} (x- x\opt)\tr \nabla^2 f(x\opt) (x-x\opt) + o(\|x - x\opt\|^2)\]
    \vspace{6cm}
\end{frame}

\begin{frame}{Analysis for quadratic functions}
    \[ f(x) = \frac{1}{2} x\tr Q x \hspace{6cm} \]
    \vspace{3cm}    \\
    Eigenvalues of $Q$:
    \vspace{3cm}
\end{frame}

\begin{frame}{Steepest descent}
    \[ f(x) = \frac{1}{2} x\tr Q x \qquad \nabla f(x) = Q x \qquad \nabla^2 f(x) = Q \]
    \vspace{6cm}
\end{frame}

\begin{frame}{Convergence rate}
    \[ \| x^{k+1} \|^2 \le \max \{ |1 - \alpha^k m| , |1 - \alpha^k M| \}\]
    \vspace{6cm}
\end{frame}

\begin{frame}{Condition number: Convergence rate}
    Condition number of $Q$ is $c = M/n$
    \vspace{6cm}
\end{frame}

\begin{frame}{Nesterov's method}
    \begin{align*}
        y^k &= x^k + \beta^k (x^k - x^{k-1}) \\
        x^{k+1} &= y^k - \alpha^k \nabla f(y^k)
    \end{align*}
    \vspace{6cm}
\end{frame}

\end{document}