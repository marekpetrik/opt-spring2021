\documentclass[9pt]{beamer}

\usepackage{amsmath}
\usepackage{amsfonts}
%\usepackage{eucal}
\usepackage{calrsfs}

\usepackage{array}
%\usepackage{enumitem}


\title{Mathematical Optimization: Gradient Descent}
\subtitle{CS/MATH 757/857}
\author{Marek Petrik}
\date{February 8}

\newcommand{\opt}{^\star}
\newcommand{\tr}{^\top}
\newcommand{\st}{\quad\operatorname{s.t.}\quad}
\newcommand{\eye}{I}
\newenvironment{mprog}{\begin{array}{>{\displaystyle}l>{\displaystyle}l>{\displaystyle}l}}{\end{array}}
\newcommand{\subjectto}{\operatorname{s.\,t.} &}
\newcommand{\minimize}[1]{\min_{#1} &}
\newcommand{\maximize}[1]{\max_{#1} &}
\newcommand{\cs}{\\[1ex] & }
\newcommand{\stc}{\\[1ex] \subjectto}
\newcommand{\real}{\mathbb{R}}


\setbeamertemplate{navigation symbols}{}
\usefonttheme{professionalfonts}
\setbeamertemplate{footline}[frame number]

\begin{document}

\begin{frame}
	\maketitle
\end{frame}

\begin{frame}{Last Time}
    \begin{enumerate}
        \item Necessary Optimality Conditions
        \vfill
        \item Sufficient Optimality Conditions
    \end{enumerate}
\end{frame}

\begin{frame}{Are we done yet?}
    \[  \min_{x \in \real^2} \; \log(1 + \exp(2 x_1)) + \log(1 + \exp(-3 x_2)) \]
    \vspace{7cm}
\end{frame}

\begin{frame}{Today: Gradient Descent}
    \[ x^{k+1} \;=\; x^k - \alpha^k D^k \nabla f(x^k)\]
    \begin{enumerate}
        \item \textbf{Direction}
        \vfill
        \item Step size
        \vfill
        \item Convergence
    \end{enumerate}
\end{frame}

\begin{frame}{Basic Idea}
    \[ x_\alpha = x - \alpha \nabla f(x),\; \alpha \ge 0 \]
    \vspace{5cm}
\end{frame}

\begin{frame}{First order expansion}
    \[ f(x_\alpha)  = f(x) + \nabla f(x)\tr (x_\alpha - x) + o( \| x_\alpha - x \|)\]
    \vspace{5cm}
    \[ x_\alpha = x - \alpha \nabla f(x),\; \alpha \ge 0 \]
\end{frame}

\begin{frame}{A more general direction?}
    \[ x_\alpha = x - \alpha d,\; \alpha \ge 0, \qquad d\tr \nabla f(x) < 0\]
    \vspace{3cm}
    \[ f(x_\alpha)  = f(x) + \nabla f(x)\tr (x_\alpha - x) + o( \| x_\alpha -
    x \|)\]
        \vspace{3cm}
\end{frame}

\begin{frame}{Gradient direction}
    \begin{enumerate}
        \item Steepest descent (minimizes the direction)
        \[ x^{k+1} = x^k - \alpha^k \nabla f(x^k) \]
        \vfill
        \item Newton's method
        \[ x^{k+1} = x^k - (\nabla^2 f(x^k))^{-1} \alpha^k \nabla f(x^k) \]
        \vfill
        \item Many others: Natural gradient, scaled steepest descent, modified Newtons method, \ldots
    \end{enumerate}
\end{frame}

\begin{frame}{Today: Gradient Descent}
    \[ x^{k+1} \;=\; x^k - \alpha^k D^k \nabla f(x^k)\]
    \begin{enumerate}
        \item Direction
        \vfill
        \item \textbf{Step size}
        \vfill
        \item Convergence
    \end{enumerate}
\end{frame}

\begin{frame}{(Limited) minimization rule}
    \[ f(x^k + \alpha^k d^k) = \min_{\alpha\ge 0} f(x^k + \alpha d^k) \]
        \vspace{5cm}
\end{frame}

\begin{frame}{Successive stepsize reduction: Armijo rule}
    \[ f(x^k + \beta^m s d^k) \;\le\; f(x^k) + \sigma \beta^m s \nabla f(x^k)\tr d^k \]
    \vspace{5cm}
\end{frame}

\begin{frame}{Constant stepsize}
    \[ x^{k+1} = x^k - \alpha^k d^k \]
    \vfill
    \[ \alpha^k = s\]
\end{frame}


\begin{frame}{Constant stepsize}
    \[ x_{k+1} = x^k - \alpha^k d^k \]
    \vfill
    \[ \alpha^k = s\]
\end{frame}

\begin{frame}{Diminishing stepsize}
    \[ x_{k+1} = x^k - \alpha^k d^k \]
\vfill
    \[ \alpha^k \to 0, \qquad \sum_{k=0}^\infty \alpha^k = 0\]
\end{frame}

\begin{frame}{Termination}
    \[ \| \nabla f(x^k) \| \le \epsilon  \]
    \vspace{4cm}
    \[  \frac{\| \nabla f(x^k) \|}{\| \nabla f(x^0) \|} \le \epsilon \]
\end{frame}

\begin{frame}{How meaningful is termination condition?}
    \[ f(x) = \frac{1}{2} \cdot \epsilon \cdot x \]
    \vspace{3cm}
    \[ \frac{z\tr \nabla^2 f(x) z}{\|z\|^2} \ge m \]
    Then
    \[ \| x - x\opt \| \le \frac{\epsilon}{m}, \qquad f(x) - f(x\opt) \le \frac{\epsilon^2}{m} \]
\end{frame}

\begin{frame}{Today: Gradient Descent}
    \[ x^{k+1} \;=\; x^k - \alpha^k D^k \nabla f(x^k)\]
    \begin{enumerate}
        \item Direction
        \vfill
        \item Step size
        \vfill
        \item \textbf{Convergence}
    \end{enumerate}
\end{frame}

\begin{frame}{Line search}
    If $\nabla f(x^k) d^k < 0$ for nonstationary points \textbf{and} $\alpha^k$ is chosen by (limited) minimization rule or Armijo rule, then (if it exists):
    \[ \lim_{k\to\infty} \nabla f(x^k) = 0 ~.\]
    \vspace{5cm}
\end{frame}

\begin{frame}{Constant stepsize}
    We get (if it exists)
    \[ \lim_{k\to\infty} \nabla f(x^k) = 0 \]
    when
    \begin{enumerate}
        \item Lipschitz condition:
        \[ \nabla f(x) - \nabla f(y) \le L \| x - y \| \]
        \item $\epsilon \le \alpha^k \le (2-\epsilon) \bar{\alpha}^k$ and
        \[ \bar{\alpha}^k = \frac{|\nabla f(x^k)\tr d^k|}{L \cdot d^k} \]
    \end{enumerate}
\end{frame}


\begin{frame}{Diminishing stepsize}
    We get (if it exists)
    \[ \lim_{k\to\infty} \nabla f(x^k) = 0 \]
    when
    \begin{enumerate}
        \item Lipschitz condition:
        \[ \nabla f(x) - \nabla f(y) \le L \| x - y \| \]
        \item Exist $c_1 > 0$ and $c_2 > 0$ such that
        \[ c_1 \| \nabla f(x^k) \|^2 \le -\nabla f(x^k)\tr d^k, \qquad \|d^k\|^2 \le c_2 \| f(x^k) \|^2\]
        \item Stepsize satisfies:
        \[ \alpha^k \to 0, \qquad \sum_{k=0}^{\infty} \alpha^k = \infty \]
    \end{enumerate}
\end{frame}

\begin{frame}{Capture theorem}
    If $f(x^{k+1}) \le f(x^k)$ and $x^{k+1} = x^k + \alpha^k d^k$ and $x^k \to $ stationary point and
    \begin{enumerate}
        \item Exist $s >0 $ and $c > 0$ such that $\forall k$
        \[ \alpha^k \le s, \qquad \| d^k \| \le \| \nabla f(x^k) \| \]
        \item $x\opt$ is a stationary point in some open $\mathcal{S}$
        \item $x^{\bar{k}} \in \mathcal{S}$ for some $\bar{k}$
    \end{enumerate}
    Then:
    \[
    x^k \to x\opt~.
    \]

\end{frame}

\end{document}