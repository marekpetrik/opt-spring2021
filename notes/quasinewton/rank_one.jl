using LinearAlgebra;

function D_next(Dᵏ, pᵏ, qᵏ)
    yᵏ = pᵏ - Dᵏ * qᵏ;
    return Dᵏ + ( yᵏ * yᵏ') / (qᵏ' * yᵏ);
end

# convex function: 1/2 * x' Q x - c' x + exp(x)
Q = [1 0.4; 0.4 2];
c = [15; 5];

function value(xᵏ)
    return 0.5 * xᵏ' * Q * xᵏ - c' * xᵏ + sum(exp.(xᵏ));
end

function derivative(xᵏ)
    return (Q * xᵏ - c + exp.(xᵏ));
end

# assume a fixed step size?
function x_next(Dᵏ, xᵏ)
    dᵏ = - Dᵏ * derivative(xᵏ);
    β = range(0.0001,2, length = 1000); # candidates
    fs = [value(xᵏ + a * dᵏ) for a in β ];
    α  = β[argmin(fs)];
    xˡ = xᵏ + α * dᵏ;
    return xˡ;
end

function pqᵏ(xᵏ, xˡ)
    return (xˡ - xᵏ, derivative(xˡ) - derivative(xᵏ));
end

D⁰ = [1 0.4; 0.4 2];
x⁰ = [1; 1];

# initial point
x¹ = x_next(D⁰, x⁰)
(p⁰, q⁰) = pqᵏ(x⁰, x¹)

# step 1
D¹ = D_next(D⁰, p⁰, q⁰);
x² = x_next(D¹, x¹)
println("step 1:", norm(D¹ * q⁰ - p⁰));

# step 2
(p¹, q¹) = pqᵏ(x¹, x²)
D² = D_next(D¹, p¹, q¹);
x³ = x_next(D¹, x¹)
println("step 2 (0):", norm(D² * q⁰ - p⁰));
println("step 2 (1):", norm(D² * q¹ - p¹));

p¹' * q⁰ - q¹' * p⁰
